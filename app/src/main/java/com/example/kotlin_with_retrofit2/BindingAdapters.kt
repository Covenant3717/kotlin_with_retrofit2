package com.example.kotlin_with_retrofit2

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

// Kotlin-файл для кастомных параметров для биндинга в xml-разметку

@BindingAdapter("app:imageUrl")
fun loadImage(view: ImageView, url: String) {
    view.visibility = View.VISIBLE
    Picasso.get().load("https://image.tmdb.org/t/p/original/$url").into(view)
}