package com.example.kotlin_with_retrofit2

import android.app.ProgressDialog
import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast
import java.time.Duration
import android.net.NetworkInfo
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso


val Context.checkNetwork: Boolean
    get() {
        return (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
            .activeNetworkInfo?.isConnected == true
    }

fun Context.liteToast(text: String, duration: Int = Toast.LENGTH_SHORT) = Toast.makeText(this, text, duration)

fun Context.showProgressDialog(): ProgressDialog {
    val pd = ProgressDialog(this)
    pd.setMessage("Wait...")
    pd.show()
    return pd
}

fun Context.checkNetworkAvailable(): Boolean {
    val connectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetworkInfo = connectivityManager.activeNetworkInfo
    return activeNetworkInfo != null && activeNetworkInfo.isConnected
}


