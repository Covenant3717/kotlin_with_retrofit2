package com.example.kotlin_with_retrofit2.network

import android.content.Context
import android.util.Log
import com.example.kotlin_with_retrofit2.checkNetworkAvailable
import com.example.kotlin_with_retrofit2.liteToast
import com.example.kotlin_with_retrofit2.showProgressDialog
import retrofit2.HttpException

class DoRequest {


    //==================================================================================================================

    companion object {

        suspend fun requestGetPopularMovies(context: Context, api_key: String): Any {
            if (context.checkNetworkAvailable()) {
//                val pd = context.showProgressDialog()

                val request = MovieApi.Retrofit2.makeRetrofitService().getMovies(api_key)

                try {
                    val response = request.await()

                    if (response.isSuccessful) {
                        val list = response.body()?.results
                        if (list != null) {

//                            pd.dismiss()
                            return list
                        }
                    } else {
                        context.liteToast("Body ${response.code()}")
                        Log.d("ml", "Body ${response.code()}")
//                        pd.dismiss()
                    }

                } catch (e: HttpException) {
                    context.liteToast("Error: ${e.code()}")
                    Log.d("ml", "Error: ${e.code()}")
//                    pd.dismiss()
                } catch (e: Throwable) {
                    context.liteToast("Ooops: Something else went wrong, e: ${e.localizedMessage}")
                    Log.d("ml", "Ooops: Something else went wrong, e: ${e.localizedMessage}")
//                    pd.dismiss()
                }

            } else {
                context.liteToast("No internet connection")
                Log.d("ml", "No internet connection")
            }

            return Any()
        }





    }
}