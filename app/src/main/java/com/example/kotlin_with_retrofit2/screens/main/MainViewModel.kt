package com.example.kotlin_with_retrofit2.screens.main

import android.app.Application
import android.util.Log
import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.*
import com.example.kotlin_with_retrofit2.network.DoRequest
import com.example.kotlin_with_retrofit2.screens.film.pojo.Movie
import com.example.kotlin_with_retrofit2.api_key
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class MainViewModel: ViewModel() {

    private val model by lazy { Model() }
    val progress by lazy { ObservableInt(View.GONE) }
    val adapter by lazy { RecyclerMoviesAdapter() }

    //==================================================================================================================

    fun getMovies():MutableLiveData<List<Movie.Result>>{
        model.requestMovies()
        return model.getMoviesLiveData()
    }


}