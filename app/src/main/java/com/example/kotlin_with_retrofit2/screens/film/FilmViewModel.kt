package com.example.kotlin_with_retrofit2.screens.film

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.kotlin_with_retrofit2.screens.film.pojo.Movie

class FilmViewModel: ViewModel() {

    companion object {
        val filmData by lazy { MutableLiveData<Movie.Result>() }
    }


    //==================================================================================================================


}