package com.example.kotlin_with_retrofit2.screens.film.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Movie(
    @SerializedName("page") @Expose var page: Int,
    @SerializedName("total_results") @Expose var totalResults: Int,
    @SerializedName("total_pages") @Expose var totalPages: Int,
    @SerializedName("results") @Expose var results: List<Result>
) {

    inner class Result(
        @SerializedName("vote_count") @Expose var voteCount: Int,
        @SerializedName("id") @Expose var id: Int,
        @SerializedName("video") @Expose var video: Boolean,
        @SerializedName("vote_average") @Expose var voteAverage: Double,
        @SerializedName("title") @Expose var title: String,
        @SerializedName("popularity") @Expose var popularity: Double,
        @SerializedName("poster_path") @Expose var posterPath: String,
        @SerializedName("original_language") @Expose var originalLanguage: String,
        @SerializedName("original_title") @Expose var originalTitle: String,
        @SerializedName("genre_ids") @Expose var genreIds: List<Int>,
        @SerializedName("backdrop_path") @Expose var backdropPath: String,
        @SerializedName("adult") @Expose var adult: Boolean,
        @SerializedName("overview") @Expose var overview: String,
        @SerializedName("release_date") @Expose var releaseDate: String
    )
}

