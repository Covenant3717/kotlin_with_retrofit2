package com.example.kotlin_with_retrofit2.screens.film

import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.kotlin_with_retrofit2.R
import com.example.kotlin_with_retrofit2.databinding.ActivityFilmBinding
import com.squareup.picasso.Picasso


class FilmActivity : AppCompatActivity() {

    private val filmViewModel by lazy { ViewModelProviders.of(this).get(FilmViewModel::class.java) }
    private val binding by lazy { DataBindingUtil.setContentView<ActivityFilmBinding>(this, R.layout.activity_film) }

    //==================================================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initFilmData()
    }

    //==================================================================================================================

    private fun initFilmData() {
        FilmViewModel.filmData.observe(this, Observer { data ->
            binding.movie = data
        })
    }



}




