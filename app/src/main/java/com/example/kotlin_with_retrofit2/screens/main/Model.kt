package com.example.kotlin_with_retrofit2.screens.main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.kotlin_with_retrofit2.api_key
import com.example.kotlin_with_retrofit2.network.MovieApi
import com.example.kotlin_with_retrofit2.screens.film.pojo.Movie
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class Model() {

    private val movies: MutableLiveData<List<Movie.Result>> by lazy { MutableLiveData<List<Movie.Result>>() }

    //==================================================================================================================

    fun getMoviesLiveData(): MutableLiveData<List<Movie.Result>> = movies

    fun requestMovies() {
        GlobalScope.launch {
            val response = MovieApi.Retrofit2.makeRetrofitService().getMovies(api_key).await()

            if (response.isSuccessful) {
                val list = response.body()?.results
                if (list != null) {
                    movies.postValue(list)
                }
            } else {
//                context.liteToast("Body ${response.code()}")
//                Log.d("ml", "Body ${response.code()}")
            }
        }
    }






}