package com.example.kotlin_with_retrofit2.screens.main

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.kotlin_with_retrofit2.R
import com.example.kotlin_with_retrofit2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val mainViewModel by lazy { ViewModelProviders.of(this).get(MainViewModel::class.java) }
    private val binding by lazy { DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main) }

    //==================================================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.mainViewModel = mainViewModel
        binding.lifecycleOwner = this

        initRecyclerMovies()
    }

    private fun initRecyclerMovies(){
        mainViewModel.progress.set(View.VISIBLE)
        mainViewModel.getMovies().observe(this, Observer {list ->
            if (list != null && list.isNotEmpty()){
                mainViewModel.progress.set(View.GONE)
                mainViewModel.adapter.setData(this, list)
            } else{
                Log.d("ml", "list empty")
            }
        })
    }

}