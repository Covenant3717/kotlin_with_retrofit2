package com.example.kotlin_with_retrofit2.screens.main

import android.content.Context
import android.content.Intent
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlin_with_retrofit2.screens.film.FilmActivity
import com.example.kotlin_with_retrofit2.screens.film.pojo.Movie
import com.example.kotlin_with_retrofit2.R
import com.example.kotlin_with_retrofit2.screens.film.FilmViewModel
import com.squareup.picasso.Picasso
import java.util.*

class RecyclerMoviesAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var context: Context
    private var list: List<Movie.Result> = ArrayList()

    //==================================================================================================================

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var v = LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        return MyViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val current = list[position]
        (holder as MyViewHolder).bind(current)
    }

    override fun getItemCount(): Int = list.size

    fun setData(context: Context, list: List<Movie.Result>){
        this.context = context
        this.list = list
        notifyDataSetChanged()
    }

    //------------------------------------------------------------------------------------------------------------------

    private inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val imgView = itemView.findViewById<ImageView>(R.id.movie_image)
        private val tvTitle = itemView.findViewById<TextView>(R.id.movie_title)
        private val tvDate = itemView.findViewById<TextView>(R.id.movie_release_date)
        private val tvOverview = itemView.findViewById<TextView>(R.id.movie_overview)

        fun bind(current: Movie.Result) {
            imgView.setOnClickListener {
                context.startActivity(Intent(context, FilmActivity::class.java))
                FilmViewModel.filmData.value = current
            }

            Picasso.get()
                .load("https://image.tmdb.org/t/p/w500/${current.posterPath}").fit().centerCrop(Gravity.TOP)
                .placeholder(context.resources.getDrawable(R.drawable.placeholder))
                .error(context.resources.getDrawable(R.drawable.placeholder))
                .into(imgView)

            tvTitle.text = current.title
            tvDate.text = current.releaseDate
            tvOverview.text = current.overview
        }
    }


}
